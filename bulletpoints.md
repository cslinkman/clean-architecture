
Points

- Robert Martin Good Architecture Screams Intended use

- Directory Slide
The top level of an application rarely communicates what the application actually does.
The top level of the application typically shows the framework that an application is made with

- Delivery Mechanism 
The web is just an IO channel. It shouldn't be what everything is centered around

- MVC lines
When the web is what everything is centered around then you get business objects that start to take on view properties and controllers which expect business objects to do more and more.
The Controllers and the views are architecturally important. They are isolated and protected.

- Use Case example
So how to do these things in what I'm referring to as the "clean" architecture
Use Cases. Define what we want to accomplish along with the steps on how to accomplish that thing and what the result of the action is.


- Interactor
An interactor is the encoding of what is desired in the use case itself. 

- Entity
An entity has application independent business rules. 

- Boundaries
The contact that the interactor has to ensure that data matches what the interactor can act on

- User Flow
How does a user interact with this system
Explain how this is testable

- Model View Presenter
What does this look like in practice
Explain how this is also testable

- Full flow, user interaction into interactor

- What do we do about the database

- How do entities get created / hydrated
Another boundary is used to fetch the information that is desired for the interactor. The interactor then acts on those entities as needed

